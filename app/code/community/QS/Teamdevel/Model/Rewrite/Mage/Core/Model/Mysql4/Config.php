<?php
 /**
 * Team Development Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   QS
 * @package    QS_Teamdevel
 * @author     Quart-soft Magento Team <magento@quart-soft.com>
 * @copyright  Copyright (c) 2010 Quart-soft Ltd http://quart-soft.com 
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * BaseUrl locally rewrites 
 *
 * @see Mage_Core_Model_Mysql4_Config
 */
class QS_Teamdevel_Model_Rewrite_Mage_Core_Model_Mysql4_Config extends Mage_Core_Model_Mysql4_Config
{
/**
     * Load configuration values into xml config object
     *
     * @param Mage_Core_Model_Config $xmlConfig
     * @param string $cond
     * @return Mage_Core_Model_Mysql4_Config_Collection
     */
    public function loadToXml(Mage_Core_Model_Config $xmlConfig, $cond=null)
    {     
        $read = $this->_getReadAdapter();
        if (!$read) {
            return $this;
        }

        $websites = array();
        $rows = $read->fetchAssoc("select website_id, code, name from ".$this->getTable('website'));
        foreach ($rows as $w) {
            $xmlConfig->setNode('websites/'.$w['code'].'/system/website/id', $w['website_id']);
            $xmlConfig->setNode('websites/'.$w['code'].'/system/website/name', $w['name']);
            $websites[$w['website_id']] = array('code'=>$w['code']);
        }

        $stores = array();
        $rows = $read->fetchAssoc("select store_id, code, name, website_id from ".$this->getTable('store')." order by sort_order asc");
        foreach ($rows as $s) {
            $xmlConfig->setNode('stores/'.$s['code'].'/system/store/id', $s['store_id']);
            $xmlConfig->setNode('stores/'.$s['code'].'/system/store/name', $s['name']);
            $xmlConfig->setNode('stores/'.$s['code'].'/system/website/id', $s['website_id']);
            $xmlConfig->setNode('websites/'.$websites[$s['website_id']]['code'].'/system/stores/'.$s['code'], $s['store_id']);
            $stores[$s['store_id']] = array('code'=>$s['code']);
            $websites[$s['website_id']]['stores'][$s['store_id']] = $s['code'];
        }

        $subst_from = array();
        $subst_to = array();

        // load all configuration records from database, which are not inherited
        $rows = $read->fetchAll("select scope, scope_id, path, value from ".$this->getMainTable().($cond ? ' where '.$cond : ''));

        // set default config values from database
        foreach ($rows as $r) {
            if ($r['scope']!=='default') {
                continue;
            }
            
            //QS Teamdevel start
            /*****************************
            app/etc/local.xml:
            <config>
                <global>
                    ...
                    <unsecure_base_url>
                        <url><![CDATA[http://debug.your-site.com/developer1/project1/]]></url>
                    </unsecure_base_url>
                    <secure_base_url>
                        <url><![CDATA[https://debug.your-site.com/developer1/project1/]]></url>
                    </secure_base_url>
                   ...
                </global>
            </config>
            ******************************/
            if (Mage::getConfig()->getNode('global/unsecure_base_url')
              && Mage::getConfig()->getNode('global/unsecure_base_url')->url
              && $r['path']=='web/unsecure/base_url') { 
                $subst_from[/*$r['path']*/] = $r['value'];
                $subst_to[] = Mage::getConfig()->getNode('global/unsecure_base_url')->url;
            }
            if (Mage::getConfig()->getNode('global/secure_base_url')
              && Mage::getConfig()->getNode('global/secure_base_url')->url
              && $r['path']=='web/secure/base_url') {
                $subst_from[/*$r['path']*/] = $r['value'];
                $subst_to[] =  Mage::getConfig()->getNode('global/secure_base_url')->url;
            }
            //QS Teamdevel end
            
            $value = str_replace($subst_from, $subst_to, $r['value']);
            $xmlConfig->setNode('default/'.$r['path'], $value);
        }
        // inherit default config values to all websites
        $extendSource = $xmlConfig->getNode('default');
        foreach ($websites as $id=>$w) {
            $websiteNode = $xmlConfig->getNode('websites/'.$w['code']);
            $websiteNode->extend($extendSource);
        }

        $deleteWebsites = array();
        // set websites config values from database
        foreach ($rows as $r) {
            if ($r['scope']!=='websites') {
                continue;
            }
            $value = str_replace($subst_from, $subst_to, $r['value']);
            if (isset($websites[$r['scope_id']])) {
                $xmlConfig->setNode('websites/'.$websites[$r['scope_id']]['code'].'/'.$r['path'], $value);
            }
            else {
                $deleteWebsites[$r['scope_id']] = $r['scope_id'];
            }
        }

        // extend website config values to all associated stores
        foreach ($websites as $website) {
            $extendSource = $xmlConfig->getNode('websites/'.$website['code']);
            if (isset($website['stores'])) {
                foreach ($website['stores'] as $sCode) {
                    $storeNode = $xmlConfig->getNode('stores/'.$sCode);
                    /**
                     * $extendSource DO NOT need overwrite source
                     */
                    $storeNode->extend($extendSource, false);
                }
            }
        }

        $deleteStores = array();
        // set stores config values from database
        foreach ($rows as $r) {
            if ($r['scope']!=='stores') {
                continue;
            }
            $value = str_replace($subst_from, $subst_to, $r['value']);
            if (isset($stores[$r['scope_id']])) {
                $xmlConfig->setNode('stores/'.$stores[$r['scope_id']]['code'].'/'.$r['path'], $value);
            }
            else {
                $deleteStores[$r['scope_id']] = $r['scope_id'];
            }
        }

        if ($deleteWebsites) {
            $this->_getWriteAdapter()->delete($this->getMainTable(), array(
                $this->_getWriteAdapter()->quoteInto('scope=?', 'websites'),
                $this->_getWriteAdapter()->quoteInto('scope_id IN(?)', $deleteWebsites),
            ));
        }

        if ($deleteStores) {
            $this->_getWriteAdapter()->delete($this->getMainTable(), array(
                $this->_getWriteAdapter()->quoteInto('scope=?', 'stores'),
                $this->_getWriteAdapter()->quoteInto('scope_id IN(?)', $deleteStores),
            ));
        }

        return $this;
    }
}

