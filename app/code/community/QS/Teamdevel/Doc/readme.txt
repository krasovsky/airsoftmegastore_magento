This product will help the development team share the same database. 

Every developer has its own local copy of project files (he can use software for version control) and has access to a shared database of project. This module allows you to reassign the base URL of the site locally for each developer.  
Please be careful! If you are using control versions software, please do not update or ignore the configuration file (app/etc/local.xml or app/code/community/QS/Teamdevel/etc/config.xml) with local base_url.

Overriden classes:
Mage_Core_Model_Mysql4_Config::loadToXml()

Steps:
1. Download and install
2. Setup base urls in 1 of 2 files:
    app/etc/local.xml (prefered)
   or 
    app/code/community/QS/Teamdevel/etc/config.xml:

<config>
    <global>
        ...
        <unsecure_base_url>
            <url><![CDATA[http://debug.your-site.com/developer1/project1/]]></url>
        </unsecure_base_url>
        <secure_base_url>
            <url><![CDATA[https://debug.your-site.com/developer1/project1/]]></url>
        </secure_base_url>
       ...
    </global>
</config>
