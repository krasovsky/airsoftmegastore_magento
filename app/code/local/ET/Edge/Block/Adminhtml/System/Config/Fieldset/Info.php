<?php

class ET_Edge_Block_Adminhtml_System_Config_Fieldset_Info
	extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface {

		public function render(Varien_Data_Form_Element_Abstract $element) {
		    $theme_version = (string) Mage::getConfig()->getNode('modules/ET_Edge/version');
		    $plugins = array(
		        'ET Ajax' => '<a href="' . $this->getUrl('*/*/edit/section/et_ajax_configs') . '" title="ET Ajax">Ajax</a>',
		        'ET Carousel' => '<a href="' . $this->getUrl('*/*/edit/section/et_carousel_configs') . '" title="ET Ajax">Carousel</a>',
		        'ET Filter' => '<a href="' . $this->getUrl('*/*/edit/section/et_filter_configs') . '" title="ET Ajax">Filter</a>',
		        'ET Megamenu' => '<a href="' . $this->getUrl('*/*/edit/section/et_megamenu_configs') . '" title="ET Ajax">Megamenu</a>',
		        'ET Products' => '<a href="' . $this->getUrl('*/*/edit/section/et_products_configs') . '" title="ET Ajax">Products</a>',
		        'ET Slider' => '<a href="' . $this->getUrl('*/*/edit/section/et_slider_configs') . '" title="ET Ajax">Slider</a>',
		        'ET Tabs' => '<a href="' . $this->getUrl('*/*/edit/section/et_tabs_configs') . '" title="ET Ajax">Tabs</a>'
	        );
		    $plugin_urls = implode(' - ' , $plugins);
		    
        	return <<<HTML
        	<div class="section-config active">
        	<div class="entry-edit-head collapseable"><a onclick="Fieldset.toggleCollapse('edge-infor', ''); return false;" href="#" id="edge-infor-head" class="open">Ecom-Themes: Edge - v{$theme_version} </a></div>
        	<input type="hidden" value="1" name="config_state[edge-infor]" id="edge-infor-state">
        	<fieldset id="edge-infor" class="config collapseable">
	<table>
		<tr>
		<td><img src="http://img.ecomtheme.com/edge/80x80.png" alt="Ecom-Themes: Edge" /></td>
			<td width="10"></td>
			<td>
				<p><strong><a href="http://themeforest.net/item/edge-responsive-multipurpose-magento-theme/10321714/support" target="_blank">Send a Message</a></strong> about your issues.
				<br /><strong><a href="http://themeforest.net/downloads" target="_blank">Download</a></strong> latest version.
				<br /><strong><a href="#" target="_blank">Online Document</a></strong> - comming soon.
				</p>
			</td>
		</tr>
	</table>
</fieldset>
        	</div>
HTML;
    	}
    	
	}
