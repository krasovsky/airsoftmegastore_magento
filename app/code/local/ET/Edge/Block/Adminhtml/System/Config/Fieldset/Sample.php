<?php

class ET_Edge_Block_Adminhtml_System_Config_Fieldset_Sample
	extends Mage_Adminhtml_Block_Abstract implements Varien_Data_Form_Element_Renderer_Interface {

		public function render(Varien_Data_Form_Element_Abstract $element) {
		    if (strlen($code = Mage::getSingleton('adminhtml/config_data')->getStore())) // store level
		    {
		        $store_id = (int)Mage::getModel('core/store')->load($code)->getId();
		    }
		    elseif (strlen($code = Mage::getSingleton('adminhtml/config_data')->getWebsite())) // website level
		    {
		        $website_id = Mage::getModel('core/website')->load($code)->getId();
		        $store_id = (int)Mage::app()->getWebsite($website_id)->getDefaultStore()->getId();
		    }
		    else // default level
		    {
		        $store_id = 0;
		    }
		    
		    // var_dump($store_id);
		    
		    $html_before = "";
		    if ($store_id > 0){
		        $html_before = "<p><strong>Static Blocks, Pages and Configuration values will be created for this store only.</strong></p>";
		    }
		    
		    // Blocks
		    $imp_block_path = sprintf('ecom-themes/edge-samples/%s-%s', 'block', $store_id);
		    $imp_block = $this->getLayout()->createBlock('adminhtml/widget_button')
    		    ->setData(array(
    		        'label'     => $this->__('Import Static Blocks'),
    		        'onclick'   => "setLocation('" . $this->getUrl('edge/adminhtml_import/blocks', array('store_id'=>$store_id, '_current'=>true)) . "');"
    		    ));
    		if (!!Mage::getStoreConfigFlag($imp_block_path, $store_id)){
    		    $imp_block->setDisabled(true);
    		    $imp_block->setLabel("Static Blocks have been imported");
    		}
		    $imp_block_btn = '<p>' . $imp_block->toHtml() . '</p>';
		    
		    // Pages
		    $imp_page_path = sprintf('ecom-themes/edge-samples/%s-%s', 'page', $store_id);
		    $imp_page = $this->getLayout()->createBlock('adminhtml/widget_button')
    		    ->setData(array(
    		        'label'     => $this->__('Import Pages'),
    		        'onclick'   => "setLocation('" . $this->getUrl('edge/adminhtml_import/pages', array('store_id'=>$store_id, '_current'=>true)) . "');"
    		    ));
		    if (!!Mage::getStoreConfigFlag($imp_page_path, $store_id)){
		        $imp_page->setDisabled(true);
		        $imp_page->setLabel("Pages have been imported");
		    }
		    $imp_page_btn = '<p>' . $imp_page->toHtml() . '</p>';
		    
		    // Configuration
		    $imp_config_path = sprintf('ecom-themes/edge-samples/%s-%s', 'config', $store_id);
		    $imp_config = $this->getLayout()->createBlock('adminhtml/widget_button')
		    ->setData(array(
		        'label'     => $this->__('Import Configuration'),
		        'onclick'   => "setLocation('" . $this->getUrl('edge/adminhtml_import/configs', array('store_id'=>$store_id, '_current'=>true)) . "');"
		    ));
		    if (!!Mage::getStoreConfigFlag($imp_config_path, $store_id)){
		        $imp_config->setDisabled(true);
		        $imp_page->setLabel("Configuration have been imported");
		    }
		    $imp_config_btn = '<p>' . $imp_config->toHtml() . '</p>';
		    
        	return <<<HTML
        	<div class="section-config active">
        	<div class="entry-edit-head collapseable"><a onclick="Fieldset.toggleCollapse('edge-sample', ''); return false;" href="#" id="edge-sample-head" class="open">Sample Data Install</a></div>
        	<input type="hidden" value="1" name="config_state[edge-sample]" id="edge-sample-state">
        	<fieldset id="edge-sample" class="config collapseable" style="display: none;">
        	    {$html_before}
			    {$imp_block_btn}
			    {$imp_page_btn}
			    {$imp_config_btn}
            </fieldset>
        	</div>
HTML;
    	}

	}
