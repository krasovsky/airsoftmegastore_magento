<?php

Mage::log('START EDGE DATA 1.0.2 SETUP', Zend_Log::DEBUG, 'setup.log', true);
try{
    $slider_groups = Mage::getModel('slider/group')->getCollection();
    if ($slider_groups->getSize() == 0){
        $group = Mage::getModel('slider/group')->setData(array(
            'title' => 'Edge Group',
            'description' => 'Sample Group for Edge Theme',
            'state' => 1,
            'css_position' => 'relative',
            'css_width' => '575px',
            'css_height' => '455px',
            'loading_screen' => 1,
            'lazy_loading' => 1
        ))->save();
        
        if ($group_id = $group->getId()){
            $slides = array(
                array(
                    'title' => 'Slide #1',
                    'description' => 'Sample Slide #1',
                    'state' => 1,
                    'slide_image' => 'et/slider/slide/slide1.jpg',
                    'slide_transition' => 'Doors',
                    'css_position' => 'absolute'
                ),
                array(
                    'title' => 'Slide #2',
                    'description' => 'Sample Slide #2',
                    'state' => 1,
                    'slide_image' => 'et/slider/slide/slide2.jpg',
                    'slide_transition' => 'Fade',
                    'css_position' => 'absolute'
                    ),
                array(
                    'title' => 'Slide #3',
                    'description' => 'Sample Slide #3',
                    'state' => 1,
                    'slide_image' => 'et/slider/slide/slide3.jpg',
                    'slide_transition' => 'Chess Replace TB',
                    'css_position' => 'absolute'
                )
            );
            foreach ($slides as $slide_data){
                Mage::getModel('slider/slide')->setData($slide_data)->setGroupId($group_id)->save();
            }
        }
    } else {
    }
    
    $menu_positions = Mage::getModel('megamenu/position')->getCollection();
    if ($menu_positions->getSize() == 0){
        ob_start();
        echo "
        INSERT INTO `et_megamenu_position` VALUES(9, 'Default Category', '', '', 1, 1, '', '2014-09-20 20:37:03');
        INSERT INTO `et_megamenu_position` VALUES(10, 'Sidebar Menu', NULL, '', 1, 1, '', '2015-01-20 21:36:28');
        
        INSERT INTO `et_megamenu_menu` VALUES(63, 'Default Category - Root', '', NULL, '', '', '', '', '', 12, 0, 0, 0, 9, 0, 0, 0, 99, 1, '', '', '', '', '', 1, 0, 'default', '', '2015-01-21 17:51:31');
        INSERT INTO `et_megamenu_menu` VALUES(64, 'Shop', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 63, 1, 3, 86, 2, '#', NULL, 'category/4', NULL, '', 1, 0, 'fullwidth', '1170px/6', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(65, 'Book & Media', NULL, NULL, '', '', '', '', '', 1, 0, 1, 0, 9, 64, 2, 46, 63, 5, '#', NULL, 'category/60', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(66, 'Home & Kitchen', NULL, NULL, '', '', '', '', '', 1, 0, 1, 0, 9, 64, 2, 64, 83, 5, '#', NULL, 'category/57', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(67, 'Pots & Pans', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 67, 68, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(68, 'Pressure Cookers', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 69, 70, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(69, 'Kitchen Storage', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 71, 72, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(70, 'Laptop Skins & Decal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 12, 0, 1, 0, 9, 96, 3, 39, 40, 2, '#', NULL, 'category/11', NULL, NULL, 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(78, 'New Arrivals', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 63, 1, 87, 92, 2, '#', NULL, 'category/5', NULL, '', 1, 0, 'fullwidth', '1170px/6', '2015-01-21 17:51:31');
        INSERT INTO `et_megamenu_menu` VALUES(79, 'Custom HTML', NULL, '<div class=\"menu-new-arrivals\">\r\n	<div class=\"row\">\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals1.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Tablet & Smartphone</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals2.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">PC & All in One</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals3.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Washing Machines</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals4.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Bicycles & Equipments</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals5.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Men Fahions</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals6.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Women Fashions</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals7.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Washing Machines</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-3 col-md-3 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals8.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Bicycles & Equipments</a>\r\n		</div>		\r\n	</div>\r\n</div>', '', '', '', '', '', 4, 0, 1, 0, 9, 78, 2, 88, 89, 7, '#', NULL, 'category/14', NULL, '', 1, 2, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(92, 'Blog', '', NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 63, 1, 93, 94, 5, '', '', 'category/7', '', '', 1, 0, 'default', '', '2015-01-21 17:51:31');
        INSERT INTO `et_megamenu_menu` VALUES(93, 'About', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 63, 1, 95, 96, 5, NULL, NULL, 'category/8', NULL, '', 1, 0, 'default', '', '2015-01-21 17:51:31');
        INSERT INTO `et_megamenu_menu` VALUES(94, 'Contact', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 63, 1, 97, 98, 5, NULL, NULL, 'category/9', NULL, '', 1, 0, 'default', '', '2015-01-21 17:51:31');
        INSERT INTO `et_megamenu_menu` VALUES(95, 'Fashion', NULL, NULL, '', '', '', '', '', 1, 0, 1, 0, 9, 64, 2, 6, 25, 5, '#', NULL, 'category/5', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(96, 'Electrics', NULL, NULL, '', '', '', '', '', 1, 0, 1, 0, 9, 64, 2, 26, 45, 5, '#', NULL, 'category/4', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(97, 'Bar & Glassware', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 73, 74, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(98, 'Flasks & Thermoware', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 75, 76, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(101, 'Shirts Tops', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 7, 8, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(102, 'Kurta & Kurti', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 9, 10, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(103, 'Polo''s & T-Shirts', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 11, 12, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(104, 'Dresses & Skirts', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 13, 14, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(105, 'Jeans & Shorts', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 15, 16, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(106, 'External Hard Disks', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 27, 28, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(107, 'Pendrives', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 29, 30, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(108, 'PC Components', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 31, 32, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(109, 'Printers & Desktops', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 33, 34, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(110, 'Datacards & Routers', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 35, 36, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(111, 'Academic', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 47, 48, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(112, 'Professional', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 49, 50, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(113, 'Entrance Exam', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 51, 52, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(114, 'Literature & Fiction', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 53, 54, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(115, 'Indian Writing', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 55, 56, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(116, 'Softwares & Antivirus', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 41, 42, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(117, 'Headphones', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 43, 44, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(118, 'Children & Teens', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 57, 58, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(119, 'Comics & Graphic ', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 59, 60, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(120, 'Novels', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 65, 3, 61, 62, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:53');
        INSERT INTO `et_megamenu_menu` VALUES(121, 'Trousers & Capris', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 17, 18, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(122, 'Lingerie & Sleep', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 19, 20, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(123, 'Sports & Gymwear', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 21, 22, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(124, 'Winter Wear', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 95, 3, 23, 24, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:09:26');
        INSERT INTO `et_megamenu_menu` VALUES(125, 'Mouse & Keyboard', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 96, 3, 37, 38, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:10:16');
        INSERT INTO `et_megamenu_menu` VALUES(126, 'Tableware & Cutlery', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 77, 78, 2, '#', NULL, 'category/41', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(127, 'Kitchen Tools', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 65, 66, 2, '#', NULL, 'category/42', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(128, 'Bakeware', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 79, 80, 2, '#', NULL, 'category/53', NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(129, 'Lighting', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 66, 3, 81, 82, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(147, 'Menu Image', NULL, '<img src=\"{{skin url=\"images/sample/megamenu/menu2.jpg\"}}\" alt=\"\" />', '', '', '', NULL, '', 2, 0, 1, 0, 9, 78, 2, 90, 91, 7, NULL, NULL, NULL, NULL, '', 1, 2, 'default', '', '2015-01-21 17:51:31');
        INSERT INTO `et_megamenu_menu` VALUES(148, 'Menu Image', NULL, '<img src=\"{{skin url=\"images/sample/megamenu/menu1.jpg\"}}\" alt=\"\" />', '', '', '', '', '', 2, 0, 1, 0, 9, 64, 2, 84, 85, 7, NULL, NULL, NULL, NULL, '', 1, 2, 'default', '', '2015-01-14 10:11:36');
        INSERT INTO `et_megamenu_menu` VALUES(149, 'Hot Key', NULL, '<div class=\"hotkey-wrap\">\r\n<span>Hot Keywords</span>\r\n <ul>\r\n<li><a href=\"#\">iPhone 6 Plus</a></li>\r\n<li><a href=\"#\">Sony Xperia Z Ultra</a></li>\r\n<li><a href=\"#\">Chrismast</a></li>\r\n<li><a href=\"#\">Noel Gift</a></li>\r\n</ul>\r\n</div>', NULL, NULL, NULL, NULL, NULL, 6, 0, 1, 0, 9, 64, 2, 4, 5, 7, NULL, NULL, NULL, NULL, NULL, 1, 2, 'default', '', '2015-01-14 10:06:58');
        INSERT INTO `et_megamenu_menu` VALUES(156, 'Home', NULL, NULL, '', '', '', '', '', 12, 0, 1, 0, 9, 63, 1, 1, 2, 6, NULL, NULL, NULL, '2', '', 1, 0, 'default', '220px/12', '2014-10-12 17:00:07');
        INSERT INTO `et_megamenu_menu` VALUES(157, 'Sidebar Menu - Root', '', NULL, '', '', '', '', '', 12, 0, 0, 0, 10, 0, 0, 0, 87, 1, '', '', '', '', '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(158, 'Electronics', NULL, NULL, '', '', '', 'fa fa-laptop', '', 12, 0, 1, 0, 10, 157, 1, 1, 6, 5, NULL, NULL, 'category/4', NULL, '', 1, 0, 'default', '901px/6', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(159, 'Auto & Bike', NULL, NULL, '', '', '', 'fa fa-car', '', 12, 0, 1, 0, 10, 157, 1, 7, 8, 5, NULL, NULL, 'category/6', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(160, 'Fashion', NULL, NULL, '', '', '', 'fa fa-female', '', 12, 0, 1, 0, 10, 157, 1, 9, 72, 5, NULL, NULL, 'category/5', NULL, '', 1, 0, 'default', '901px/12', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(161, 'Book & Audios', NULL, NULL, '', '', '', 'fa fa-book', '', 12, 0, 1, 0, 10, 157, 1, 73, 74, 5, NULL, NULL, 'category/60', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(162, 'Home & Garden', NULL, NULL, '', '', '', 'fa fa-home', '', 12, 0, 1, 0, 10, 157, 1, 75, 76, 5, NULL, NULL, 'category/57', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(163, 'Sport & Outdoors', NULL, NULL, '', '', '', 'fa fa-pied-piper-alt', '', 12, 0, 1, 0, 10, 157, 1, 77, 78, 5, NULL, NULL, 'category/62', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(164, 'Beauty & Health', NULL, NULL, '', '', '', 'fa fa-leaf', '', 12, 0, 1, 0, 10, 157, 1, 79, 80, 5, NULL, NULL, 'category/58', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(165, 'Toy, Gifts & Flower', NULL, NULL, '', '', '', 'fa fa-gift', '', 12, 0, 1, 0, 10, 157, 1, 81, 82, 5, NULL, NULL, 'category/59', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(166, 'Cameras & Photo', NULL, NULL, '', '', '', 'fa fa-camera-retro', '', 12, 0, 1, 0, 10, 157, 1, 83, 84, 5, NULL, NULL, 'category/61', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(167, 'Jewelry & Watches', NULL, NULL, '', '', '', 'fa fa-clock-o', '', 12, 0, 1, 0, 10, 157, 1, 85, 86, 5, NULL, NULL, 'category/63', NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(168, 'Women', NULL, NULL, '', '', '', NULL, '', 3, 0, 1, 0, 10, 160, 2, 10, 29, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(169, 'Men', NULL, NULL, '', '', '', NULL, '', 3, 0, 1, 0, 10, 160, 2, 30, 51, 2, NULL, NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(170, 'Baby & Kids', NULL, NULL, '', '', '', NULL, '', 3, 0, 1, 0, 10, 160, 2, 52, 69, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(171, 'Shirts Tops', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 11, 12, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(172, 'T-Shirts', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 31, 32, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(173, 'Infant Wear', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 53, 54, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(174, 'Baby Winter Must Haves', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 55, 56, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(175, 'Baby Gear', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 57, 58, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(176, 'Baby Bedding', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 59, 60, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(177, 'Baby Care Combos', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 61, 62, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(178, 'Baby Grooming', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 63, 64, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(179, 'Bath & Skin Care', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 65, 66, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(180, 'Feeding & Nursing', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 170, 3, 67, 68, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(181, 'Kurta & Kurti', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 13, 14, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(182, 'Polo''s & T-Shirts', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 15, 16, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(183, 'Dresses & Skirts', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 17, 18, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(184, 'Jeans & Shorts', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 19, 20, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(185, 'Trousers & Capris', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 21, 22, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(186, 'Lingerie & Sleep', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 23, 24, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(187, 'Sports & Gymwear', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 25, 26, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(188, 'Winter Wear', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 168, 3, 27, 28, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(189, 'Casual & Party Wear ', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 33, 34, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(190, 'Shirts', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 35, 36, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(191, 'Formal Shirts', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 37, 38, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(192, 'Jeans', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 39, 40, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(193, 'Cargos, Shorts ', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 41, 42, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(194, 'Trousers', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 43, 44, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(195, 'Suits and Blazers', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 45, 46, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(196, 'Ethnic Wear', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 47, 48, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(197, 'Inner & Sleep Wear', NULL, NULL, '', '', '', NULL, '', 12, 0, 1, 0, 10, 169, 3, 49, 50, 2, '#', NULL, NULL, NULL, '', 1, 0, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(198, 'Menu Image', NULL, '<img src=\"{{skin url=\"images/sample/megamenu/menu3.jpg\"}}\" alt=\"\" />', '', '', '', NULL, '', 3, 0, 1, 0, 10, 160, 2, 70, 71, 7, NULL, NULL, NULL, NULL, '', 1, 2, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(199, 'Custom HTML', NULL, '<div class=\"menu-new-arrivals\">\r\n	<div class=\"row\">\r\n		<div class=\"menu-new-arrivals-item col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals1.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Tablet-Smartphone</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals2.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">PC & All in One</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals3.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Washing Machines</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals9.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Smartphone</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals10.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Televisions</a>\r\n		</div>\r\n		<div class=\"menu-new-arrivals-item col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n			<img src=\"{{skin url=''images/sample/megamenu/new-arrivals11.jpg''}}\" alt=\"\" />\r\n			<a href=\"#\">Headphone</a>\r\n		</div>		\r\n	</div>\r\n</div>', '', '', '', NULL, '', 4, 0, 1, 0, 10, 158, 2, 2, 3, 7, NULL, NULL, NULL, NULL, '', 1, 2, 'default', '', '2015-01-31 15:39:12');
        INSERT INTO `et_megamenu_menu` VALUES(200, 'Menu Image', NULL, '<img src=\"{{skin url=\"images/sample/megamenu/menu4.jpg\"}}\" alt=\"\" />', '', '', '', NULL, '', 2, 0, 1, 0, 10, 158, 2, 4, 5, 7, NULL, NULL, NULL, NULL, '', 1, 2, 'default', '', '2015-01-31 15:39:12');
        ";
        $sql = ob_get_clean();
        Mage::getSingleton('core/resource')->getConnection('core_write')->exec($sql);
    }
    
} catch(Exception $e){
    // die($e->getMessage());
}
Mage::log('END EDGE DATA 1.0.2 SETUP', Zend_Log::DEBUG, 'setup.log', true);