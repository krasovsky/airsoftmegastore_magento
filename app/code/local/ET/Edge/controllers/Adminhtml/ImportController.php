<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2015 EcomTheme. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Adminhtml_ImportController extends Mage_Adminhtml_Controller_Action {
    
    public function indexAction() {
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit/section/ultimo/"));

    }

    public function blocksAction() {
        $overwrite = Mage::helper('edge')->getConfig('overwrite_blocks');
        $section = $this->getRequest()->getParam('section');
        $website = $this->getRequest()->getParam('website');
        $store   = $this->getRequest()->getParam('store');
        $store_id= $this->getRequest()->getParam('store_id');
        try {
            $filename = Mage::getModuleDir('etc', 'ET_Edge') .'/sampledata/blocks.xml';
            if (!is_readable($filename)) {
                throw new Exception(Mage::helper('edge')->__("Can't read data file: %s", $filename));
            }
            $xml = new Varien_Simplexml_Config($filename);
            	
            $conflictingOldItems = array();
            $imported = 0;
            foreach ($xml->getNode('blocks')->children() as $node) {
                //Check if block already exists
                $oldBlocks = Mage::getModel('cms/block')->getCollection()
                    ->addFieldToFilter('identifier', $node->identifier)
                    ->load();
        
                //If items can be overwritten
                if (count($oldBlocks) > 0) {
                    $conflictingOldItems[] = $node->identifier;
                    if (!$overwrite) continue;
                    foreach ($oldBlocks as $old) {
                        $old->delete();
                    }
                }
                
                $node_store_ids = array(0);
                if ($store_id){
                    $node_store_ids = array($store_id);
                }
                
                Mage::getModel('cms/block')
    				->setTitle($node->title)
    				->setContent($node->content)
    				->setIdentifier($node->identifier)
    				->setIsActive($node->is_active)
    				->setStores($node_store_ids)
    				->save();
                $imported++;
        
            }
            	
            $imported
                ? Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('edge')->__('Number of imported items: %s', $imported))
                : Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('edge')->__('No items were imported'));
            	
            if ($conflictingOldItems) {
                $overwrite
                    ? Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('edge')->__('Items (%s) with the following identifiers were overwritten:<br />%s', count($conflictingOldItems), implode(', ', $conflictingOldItems)))
                    : Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('edge')->__('Unable to import items (%s) with the following identifiers (they already exist in the database):<br />%s', count($conflictingOldItems), implode(', ', $conflictingOldItems)));
            }
            
            if ($imported){
                $imp_block_path = sprintf('ecom-themes/edge-samples/%s-%s', 'block', $store_id);
                $configModel = new Mage_Core_Model_Config();
                $configModel->saveConfig($imp_block_path, "1", $store_id ? 'stores' : 'default', $store_id);
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }
        
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit", array('section'=>$section, 'website'=>$website, 'store'=>$store)));
    }

    public function pagesAction() {
        $overwrite = 1;// Mage::helper('edge')->getConfig('overwrite_pages');
        $section = $this->getRequest()->getParam('section');
        $website = $this->getRequest()->getParam('website');
        $store   = $this->getRequest()->getParam('store');
        $store_id= $this->getRequest()->getParam('store_id');
        try {
			$filename = Mage::getModuleDir('etc', 'ET_Edge') .'/sampledata/pages.xml';
			if (!is_readable($filename)) {
				throw new Exception(Mage::helper('edge')->__("Can't read data file: %s", $filename));
			}
			$xml = new Varien_Simplexml_Config($filename);
			
			$conflictingOldItems = array();
			$imported = 0;
			foreach ($xml->getNode('pages')->children() as $node) {
				//Check if block already exists
				$oldBlocks = Mage::getModel('cms/page')->getCollection()
					->addFieldToFilter('identifier', $node->identifier)
					->load();
				
				//If items can be overwritten
				if (count($oldBlocks) > 0) {
				    $conflictingOldItems[] = $node->identifier;
				    if (!$overwrite) continue;
				    foreach ($oldBlocks as $old) {
				        $old->delete();
				    }
				}
				
			    $node_store_ids = array(0);
                if ($store_id){
                    $node_store_ids = array($store_id);
                }
				
				Mage::getModel('cms/page')
					->setTitle($node->title)
					->setRootTemplate($node->root_template)
					->setContentHeading($node->content_heading)
					->setContent($node->content)
					->setLayoutUpdateXml($node->layout_update_xml)
					->setIdentifier($node->identifier)
					->setIsActive($node->is_active)
					->setStores($node_store_ids)
					->save();
			    $imported++;
				
			}
			
			$imported
			    ? Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('edge')->__('Number of imported items: %s', $imported))
			    : Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('edge')->__('No items were imported'));
			
			if ($conflictingOldItems) {
			    $overwrite
			        ? Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('edge')->__('Items (%s) with the following identifiers were overwritten:<br />%s', count($conflictingOldItems), implode(', ', $conflictingOldItems)))
			        : Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('edge')->__('Unable to import items (%s) with the following identifiers (they already exist in the database):<br />%s', count($conflictingOldItems), implode(', ', $conflictingOldItems)));
			}
			
			if ($imported){
			    $imp_page_path = sprintf('ecom-themes/edge-samples/%s-%s', 'page', $store_id);
                $configModel = new Mage_Core_Model_Config();
                $configModel->saveConfig($imp_page_path, "1", $store_id ? 'stores' : 'default', $store_id);
            }
		} catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			Mage::logException($e);
		}

        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit", array('section'=>$section, 'website'=>$website, 'store'=>$store)));
    }
    
    public function configsAction() {
        $section = $this->getRequest()->getParam('section');
        $website = $this->getRequest()->getParam('website');
        $store   = $this->getRequest()->getParam('store');
        $store_id= $this->getRequest()->getParam('store_id');
        $configModel = new Mage_Core_Model_Config();
        
        try {
            $filename = Mage::getModuleDir('etc', 'ET_Edge') .'/sampledata/default_configs.xml';
            if (!is_readable($filename)) {
                throw new Exception(Mage::helper('edge')->__("Can't read data file: %s", $filename));
            }
            $xml = new Varien_Simplexml_Config($filename);
            	
            $conflictingOldItems = array();
            $imported = 0;
            foreach ($xml->getNode('configs')->children() as $node) {
                $config_path = (string)$node->path;
                $config_value = (string)$node->value;
                
                if ($config_path){
                    $configModel->saveConfig($config_path, $config_value, $store_id ? 'stores' : 'default', $store_id);
                    $imported++;
                }
            }
            	
            $imported
                ? Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('edge')->__('Number of saved values: %s', $imported))
                : Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('edge')->__('No config values were saved'));
            	
            if ($imported){
                $imp_config_path = sprintf('ecom-themes/edge-samples/%s-%s', 'config', $store_id);
                $configModel->saveConfig($imp_config_path, "1", $store_id ? 'stores' : 'default', $store_id);
                
                $configModel->saveConfig('design/package/name', "et_edge", $store_id ? 'stores' : 'default', $store_id);
                
                // change to default logo
                $configModel->saveConfig('design/header/logo_src', "images/logo.png", $store_id ? 'stores' : 'default', $store_id);
                $configModel->saveConfig('design/header/logo_src_small', "images/logo.png", $store_id ? 'stores' : 'default', $store_id);
                
            }
            
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }
        
        $this->getResponse()->setRedirect($this->getUrl("adminhtml/system_config/edit", array('section'=>$section, 'website'=>$website, 'store'=>$store)));
    }
}
