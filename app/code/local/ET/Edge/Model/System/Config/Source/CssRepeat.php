<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Ecom-Themes. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Model_System_Config_Source_CssRepeat {

    public function toOptionArray()
	{
		return array(
			array('value'=>'repeat', 'label'=>Mage::helper('edge')->__('repeat')),
			array('value'=>'repeat-x', 'label'=>Mage::helper('edge')->__('repeat-x')),
			array('value'=>'repeat-y', 'label'=>Mage::helper('edge')->__('repeat-y')),
			array('value'=>'no-repeat', 'label'=>Mage::helper('edge')->__('no-repeat'))
		);
	}
}
