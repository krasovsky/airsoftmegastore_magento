<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Ecom-Themes. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Model_System_Config_Source_FontFamilies {
    
	public function toOptionArray(){
		return array(
			array('value' => '', 'label' => Mage::helper('edge')->__('No select')),
		    // Sans-Serif
		    array('value' => 'Arial, sans-serif', 'label' => Mage::helper('edge')->__('Arial, sans-serif')),
		    array('value' => 'Helvetica, sans-serif', 'label' => Mage::helper('edge')->__('Helvetica, sans-serif')),
		    array('value' => 'Gill Sans, sans-serif', 'label' => Mage::helper('edge')->__('Gill Sans, sans-serif')),
		    array('value' => 'Lucida, sans-serif', 'label' => Mage::helper('edge')->__('Lucida, sans-serif')),
		    array('value' => 'Helvetica Narrow, sans-serif', 'label' => Mage::helper('edge')->__('Helvetica Narrow, sans-serif')),
		    array('value' => 'sans-serif', 'label' => Mage::helper('edge')->__('sans-serif')),
		    // Serif
		    array('value' => 'Georgia, serif', 'label' => Mage::helper('edge')->__('Georgia, serif')),
		    array('value' => 'Times, serif', 'label' => Mage::helper('edge')->__('Times, serif')),
		    array('value' => 'Times New Roman, serif', 'label' => Mage::helper('edge')->__('Times New Roman, serif')),
		    array('value' => 'Palatino, serif', 'label' => Mage::helper('edge')->__('Palatino, serif')),
		    array('value' => 'Bookman, serif', 'label' => Mage::helper('edge')->__('Bookman, serif')),
		    array('value' => 'New Century Schoolbook, serif', 'label' => Mage::helper('edge')->__('New Century Schoolbook, serif')),
		    array('value' => 'serif', 'label' => Mage::helper('edge')->__('serif')),
		    // Monospace
		    array('value' => 'Andale Mono, monospace', 'label' => Mage::helper('edge')->__('Andale Mono, monospace')),
		    array('value' => 'Courier New, monospace', 'label' => Mage::helper('edge')->__('Courier New, monospace')),
		    array('value' => 'Courier, monospace', 'label' => Mage::helper('edge')->__('Courier, monospace')),
		    array('value' => 'Lucidatypewriter, monospace', 'label' => Mage::helper('edge')->__('Lucidatypewriter, monospace')),
		    array('value' => 'Fixed, monospace', 'label' => Mage::helper('edge')->__('Fixed, monospace')),
		    array('value' => 'monospace', 'label' => Mage::helper('edge')->__('monospace')),
		    // Cursive
		    array('value' => 'Comic Sans, Comic Sans MS, cursive', 'label' => Mage::helper('edge')->__('Comic Sans, Comic Sans MS, cursive')),
		    array('value' => 'Zapf Chancery, cursive', 'label' => Mage::helper('edge')->__('Zapf Chancery, cursive')),
		    array('value' => 'Coronetscript, cursive', 'label' => Mage::helper('edge')->__('Coronetscript, cursive')),
		    array('value' => 'Florence, cursive', 'label' => Mage::helper('edge')->__('Florence, cursive')),
		    array('value' => 'Parkavenue, cursive', 'label' => Mage::helper('edge')->__('Parkavenue, cursive')),
		    array('value' => 'cursive', 'label' => Mage::helper('edge')->__('cursive')),
		    // Fantasy
		    array('value' => 'Impact, fantasy', 'label' => Mage::helper('edge')->__('Impact, fantasy')),
		    array('value' => 'Arnoldboecklin, fantasy', 'label' => Mage::helper('edge')->__('Arnoldboecklin, fantasy')),
		    array('value' => 'Oldtown, fantasy', 'label' => Mage::helper('edge')->__('Oldtown, fantasy')),
		    array('value' => 'Blippo, fantasy', 'label' => Mage::helper('edge')->__('Blippo, fantasy')),
		    array('value' => 'Brushstroke, fantasy', 'label' => Mage::helper('edge')->__('Brushstroke, fantasy')),
		    array('value' => 'fantasy', 'label' => Mage::helper('edge')->__('fantasy'))
		);
	}
}
