<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Ecom-Themes. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Model_System_Config_Source_BgStyle {

    public function toOptionArray()
	{
		return array(
			array('value'=>'', 'label'=>Mage::helper('edge')->__('None')),
			array('value'=>'pattern', 'label'=>Mage::helper('edge')->__('Use Pattern')),
			array('value'=>'image', 'label'=>Mage::helper('edge')->__('Use Image'))
		);
	}
}
