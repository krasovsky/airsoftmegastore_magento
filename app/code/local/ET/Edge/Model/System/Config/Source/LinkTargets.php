<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Ecom-Themes. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Model_System_Config_Source_LinkTargets {
	public function toOptionArray() {
		return array(
				array('value'=>'_self',	'label'=>Mage::helper('edge')->__('Same Window')),
				array('value'=>'_blank','label'=>Mage::helper('edge')->__('New Window')),
				array('value'=>'_popup','label'=>Mage::helper('edge')->__('Popup Window'))
		);
	}
}
