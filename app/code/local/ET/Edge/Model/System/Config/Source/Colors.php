<?php
/**
 * @package ET_Edge
 * @version 1.0.0
 * @copyright Copyright (c) 2015 Ecom-Themes. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Edge_Model_System_Config_Source_Colors {
	public function toOptionArray(){
		return array(
			array('value' => 'default',      'label' => Mage::helper('edge')->__('Default')),
			array('value' => 'green',        'label' => Mage::helper('edge')->__('Green')),
			array('value' => 'indingo',         'label' => Mage::helper('edge')->__('Indingo')),
			array('value' => 'purple',         'label' => Mage::helper('edge')->__('Purple')),
			array('value' => 'red',         'label' => Mage::helper('edge')->__('Red')),
			array('value' => 'yellow',       'label' => Mage::helper('edge')->__('Yellow')),
		);
	}
}
