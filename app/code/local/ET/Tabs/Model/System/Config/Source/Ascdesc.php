<?php
/**
 * @package ET_Tabs
 * @version 1.0.0
 * @copyright Copyright (c) 2014 EcomTheme. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Tabs_Model_System_Config_Source_Ascdesc {
	public function toOptionArray() {
		return array(
			array('value' => 'asc',  'label' => Mage::helper('tabs')->__('ASC')),
			array('value' => 'desc', 'label' => Mage::helper('tabs')->__('DESC'))
		);
	}
}
