<?php
/**
 * @package ET_Tabs
 * @version 1.0.0
 * @copyright Copyright (c) 2014 EcomTheme. (http://www.ecomtheme.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class ET_Tabs_Block_List extends Mage_Catalog_Block_Product_Abstract {
    
    const ET_TABS_CONFIG_SECTION = 'et_tabs_configs';
    
    protected $_template = 'et/tabs/default.phtml';
    protected $store_id = null;
    protected $_productCollection = null;
    
    public function __construct ($attributes=array()) {
        parent::__construct ($attributes);
        $this->_initConfig($attributes);
    }
    
    protected function _initConfig($attributes=array()){
        if (!$this->getData('config')){
            if ($store_config = Mage::getStoreConfig('et_tabs_configs')) {
                $_config = new Varien_Object();
                foreach ($store_config as $group => $fields) {
                    foreach ($fields as $name => $value) {
                        $this->setData($name, $value);
                    }
                }
            }
            is_array($attributes) && $this->addData($attributes);
            $this->setData('config', true);
        }
        return $this;
    }
    
    public function setConfig ($name, $value=null) {
        if (is_array($name)) {
            $this->addData($name);
        } else if(!empty($name)) {
            $this->setData($name, $value);
        }
        return $this;
    }
    
    public function getConfig ($name=null, $default=null) {
        if (is_null($name)) {
            return $this->getData();
        } else {
            return $this->getDataSetDefault($name, $default);
        }
    }
    
    public function getConfigFlag($name, $df=false){
        $flag = $this->getConfig($name, $df);
        return (!empty($flag) && 'false' !== $flag) ? true : false;
    }
    public function configToString(){
        $data = $this->getData();
        foreach ($data as $key => $val){
            if (in_array($key, array('config', 'type', 'module_name'))){
                unset($data[$key]);
                continue;
            }
            if (is_object($val)) unset($data[$key]);
        }
        $data_str = serialize($data);
        $data_enc = '';
        for ($i = 0; $i<strlen($data_str); $i++){
            $hex = dechex(ord($data_str[$i]));
            $data_enc .= str_pad($hex, '2', '0', STR_PAD_LEFT);
        }
        return $data_enc;
    }
    public function configFromString($str = ''){
        $data_str = '';
        for ($i = 0; $i<strlen($str); $i+=2){
            $char = hexdec(substr($str, $i, 2));
            $data_str .= chr($char);
        }
        try{
            $data = @unserialize($data_str);
            if ($data !== false){
                $this->setData($data);
            }
        } catch(Exception $e){
        }
        return true;
    }
    
    /** ********************************** */
    
    public function getCategories () {
        $data_source = $this->getConfig('data_source', 'catalog_category');
        if ( !empty($data_source) ) {
            $categoryIds = $this->getConfig($data_source);
            if ( !empty($categoryIds) ){
                return $this->_getCategories($categoryIds);
            }
        }
        return array();
    }
    
    public function createNewTab(){
        return new Varien_Object(array(
            'name'    => 'Default Name',
            'type'    => 'category/ordering',
            'config'  => new Varien_Object(),
            'active'  => false,
            'active_class' => false,
            'source'  => null,
            'content' => null
        ));
    }
    
    public function getTabs($params = array()){
        $data_source = $this->getConfig('data_source', 'catalog_category');
        $tabs = array();
        switch ($data_source) {
            default:
            case 'catalog_category':
                $categoryIds = $this->getConfig($data_source);
                $categories = $this->_getCategories($categoryIds);
                foreach ($categories as $category){
                    // var_dump( $category );
                    $tab = $this->createNewTab();
                    $tab->type = 'category';
                    $tab->name = $category->getName();
                    $tab->source = $category;
                    $tab->active = $category->getId() == $this->getConfig('preload_category');
                    
                    // tab resouce config
                    $tab->config->catalog_category = $category->getId();
                    $tab->config->child_category_include = $this->getConfig('child_category_include', 0);
                    $tab->config->include_max_depth = $this->getConfig('include_max_depth', 0);
                    $tab->config->is_new_filter = $this->getConfig('is_new_filter', 0);
                    $tab->config->is_special_filter = $this->getConfig('is_special_filter', 0);
                    $tab->config->is_featured = $this->getConfig('is_featured', 0);
                    $tab->config->order_by = $this->getConfig('order_by', 'position');
                    $tab->config->order_dir = $this->getConfig('order_dir', 'ASC');
                    // $tab->config->page = $this->getConfig('page', 1);
                    $tab->config->page_size = $this->getConfig('product_count');
                   
                    if ($tab->active) {
                        $this->setActive(true);
                    }
                    $tabs[] = $tab;
                }
                // return $this->_getCategories($categoryIds);
                break;
            case 'tabs_by_ordering':
                $order_types = $this->getConfig($data_source);
                $order_tabs = $this->_getOrders($order_types);
                foreach ($order_tabs as $order){
                    // var_dump( $category );
                    $tab = $this->createNewTab();
                    $tab->type = 'ordering';
                    $tab->name = $order->getName();
                    $tab->source = $order;
                    $tab->active = $order->getId() == $this->getConfig('preload_ordering');
                    $tab->active_class = $order->getId() == $this->getConfig('preload_ordering');
                    
                    // tab resouce config
                    // $tab->config->data_source = $this->getConfig('data_source');
                    $tab->config->catalog_category = $this->getConfig('catalog_category_ino');
                    $tab->config->child_category_include = $this->getConfig('child_category_include', 0);
                    $tab->config->include_max_depth = $this->getConfig('include_max_depth', 0);
                    $tab->config->is_new_filter = $this->getConfig('is_new_filter', 0);
                    $tab->config->is_special_filter = $this->getConfig('is_special_filter', 0);
                    $tab->config->is_featured = $this->getConfig('is_featured', 0);
                    $tab->config->order_by = $order->getId();
                    $tab->config->order_dir = $this->getConfig('order_dir', 'ASC');
                    // $tab->config->page = $this->getConfig('page', 1);
                    $tab->config->page_size = $this->getConfig('product_count');
                   
                    if ($tab->active) {
                        $this->setActive(true);
                    }
                    $tabs[] = $tab;
                }
                // return $this->_getOrders($order_types);
                break;
        }
        return $tabs;
    }
    
    public function getTabInstance($type='') {
        if (!is_string($type) || strpos($type, '/')===false) {
            return false;
        }
        $parts = explode('/', $type);
        $tab = $this->createNewTab();
        $tab->type = $parts[0];
        
        if ($parts[0] == 'category') {
            $category = Mage::getModel('catalog/category')->load($parts[1]);
            if (! $category->getId()) {
                return false;
            }
            $tab->name = $category->getName();
            $tab->source = $category;
            $tab->config->catalog_category = $category->getId();
            $tab->config->order_by = $this->getConfig('order_by', 'position');
        } else if ($parts[0] == 'ordering') {
            $override_label = $this->getConfig('tab_label_'.$parts[1]);
            if (!empty($override_label)){
                $tab->setName($override_label);
            } else {
                $tab->setName($this->__($parts[1]));
            }
            $tab->source = new Varien_Object(array(
                'id' => $parts[1]
             ));
            $tab->config->catalog_category = $this->getConfig('catalog_category_ino');
            $tab->config->order_by = $parts[1];
        }
        
        $tab->active = true;
        
        // tab resouce config
        // $tab->config->data_source = $this->getConfig('data_source');
        
        $tab->config->child_category_include = $this->getConfig('child_category_include', 0);
        $tab->config->include_max_depth = $this->getConfig('include_max_depth', 0);
        $tab->config->is_new_filter = $this->getConfig('is_new_filter', 0);
        $tab->config->is_special_filter = $this->getConfig('is_special_filter', 0);
        $tab->config->is_featured = $this->getConfig('is_featured', 0);
        // $tab->config->order_by = $this->getConfig('order_by', 'position');
        $tab->config->order_dir = $this->getConfig('order_dir', 'ASC');
        // $tab->config->page = $this->getConfig('page', 1);
        $tab->config->page_size = $this->getConfig('product_count');
        
        return $tab;
    }
    
    public function getTabContent($tab = null){
        if (is_null($tab)){
            return '';
        }
        //Mage::log('Get Tab Content');
       // Mage::log($tab->getData());
       // Mage::log($tab->active);
       // Mage::log('Get Tab Content!!!!!!');
        
        if (!isset($tab->content)) {
            $helper = Mage::helper('tabs');
            
            $collection = null;
            $ref_type = '';
            /* @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection = $this->_getProductCollection(true);
            $category_ids = $tab->config->catalog_category;
            if ($category_ids != '*') {
                //Mage::log($category_ids);
                if (is_string($category_ids)) {
                    $category_ids = explode(',', $category_ids);
                }
                $child_category_include = $tab->config->child_category_include;
                $include_max_depth      = $tab->config->include_max_depth;
                if ($child_category_include) {
                    $category_ids = $this->_getChildrenCategories($category_ids, $include_max_depth);
                }
               // Mage::log('Categories before Filter:');
                //Mage::log($category_ids);
                $collection->inCategoryFilter($category_ids);
            }
            
            $helper->isNewFilter($collection, $tab->config->is_new_filter);
            $helper->isSpecialFilter($collection, $tab->config->is_special_filter);
            $helper->isFeaturedFilter($collection, $tab->config->is_featured);
            
            $order_by = $tab->config->order_by;
            $order_dir = $tab->config->order_dir;
             
            // SORT & position fallback
            $this->_setOrder($collection, $order_by, $order_dir);
            // $collection->addAttributeToSort('position', 'ASC');
            
            $page = $tab->config->page;
            //Mage::log("Config page: " . $tab->config->page);
            if (!$page) $page = 1;
            //Mage::log("re assign page: " . $page);
            $collection->setCurPage($page);
            
            //Mage::log("Collection Size: " . $collection->getSize() );
            
            $page_size = $tab->config->page_size;
            if ($page_size) {
                $collection->setPageSize($page_size);
            }
           
            $block = $this->getLayout()->createBlock('tabs/list_content');
            if ($block){
                $tab_type = $tab->type=='category' ? $tab->type . '/' . $tab->source->getId() : '';
                $this_config = $this->getConfig();
                if (isset($this_config['type'])) unset($this_config['type']);
                if (isset($this_config['module_name'])) unset($this_config['module_name']);
                $block->setConfig($this_config);
                $block->setActive($tab->active);
                $block->setTab($tab);
                $block->setCollection($collection);
                $block->setTemplate($content_tpl = $this->_template ? str_replace('.phtml', '_content.phtml', $this->_template) : 'et/tabs/default_content.phtml');
                $tab->content = $block->toHtml();
            } else {
                $tab->content = '';
            }
        }
        return $tab->content;
    }
    
    protected function _getOrders($types=''){
        $collection = new Varien_Data_Collection();
        
        if (is_string($types)) {
            $types = preg_split('/[\s|,|;]/', $types, -1, PREG_SPLIT_NO_EMPTY) ;
        } else if (is_array($types)) {
            $types = array_map('trim', $types);
        }
        $types = array_filter($types, create_function('$s', '$_s = trim($s); return !empty($_s);'));
        if ($types) {
            $id = 0;
            foreach ($types as $type){
                $tabItem = new Varien_Object();
                $tabItem->setId($type);
                $override_label = $this->getConfig('tab_label_'.$type);
                if (!empty($override_label)){
                    $tabItem->setName($override_label);
                } else {
                    $tabItem->setName($this->__($type));
                }
                $collection->addItem($tabItem);
            }
        }
        return $collection;
    }
    
    protected function _getProductCollection ($new=false) {
        if ($new || is_null($this->_productCollection)) {
            $this->_productCollection = Mage::getResourceModel('catalog/product_collection');
            $this->_productCollection
                ->addAttributeToSelect(Mage::getSingleton('catalog/config')->getProductAttributes())
                ->addMinimalPrice()
                ->addFinalPrice()
                ->addTaxPercents()
                ->addUrlRewrite()
                ->addStoreFilter();
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection( $this->_productCollection);
            // Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection( $this->_productCollection);
        }
        return $this->_productCollection;
    }
    
    /**
     *
     * @param array $categoryIds id of categories
     * @param string $regex Pattern to find category path
     * @param boolean $load Load and return array instead of Collection
     * @return Mage_Catalog_Model_Resource_Category_Collection|array
     */
    protected function _getCategories ($categoryIds='*', $regex='^1/[0-9/]+', $load=false) {
        if (!empty($categoryIds)) {
            /* @var $collection Mage_Catalog_Model_Resource_Category_Collection */
            $collection = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToSelect('*')
                ->addIsActiveFilter();
            
            if ($categoryIds != '*') {
                $collection->addIdFilter($categoryIds);
            }
            
            if (!is_null($regex)) {
                $collection->addPathFilter($regex);
            }
            
            if (!$load) return $collection;
            return $collection->getItems();
        }
        return array();
    }
    
    /**
     *
     * @param array $categoryIds
     * @param int $max_depth
     * @param boolean $sepa
     * @return array
     */
    protected function _getChildrenCategories ($categoryIds = array(), $max_depth=0, $sepa=false) {
        $children = array();
        if (!is_array($categoryIds)) settype($categoryIds, 'array');
        if (!is_array($categoryIds) || $max_depth<1) return $categoryIds;
        foreach ($categoryIds as $category) {
            // get sub category, limit by max_depth
            if (! $category instanceof Mage_Catalog_Model_Category) {
                $category = Mage::getModel('catalog/category')->load((int)$category);
                if (!$category->getId()) continue;
            }
            $child_ids = array($category->getId());
            if ($max_depth) {
                $max_of_max = 255;
                if ($max_depth > $max_of_max){
                    $max_depth = $max_of_max;
                }
                $query = '^' . $category->path . "(/[0-9]+){0,$max_depth}$";
                $_collection = $this->_getCategories('*', $query, 0);
                if ($_collection instanceof Mage_Catalog_Model_Resource_Category_Collection) {
                    $child_ids = $_collection->getAllIds();
                }
            }
            if ($sepa) {
                $children[$category->getId()] = $child_ids;
            } else {
                $children = array_merge($children, $child_ids);
            }
        }
        $children = array_unique($children);
        return $children;
    }
    
    protected function _getCategoryItems ($category_ids = '*') {
        /* @var $helper ET_Carousel_Helper_Data */
        $helper = Mage::helper('carousel');
        
        $collection = $this->_getProductCollection();

        if ($category_ids != '*') {
            if (is_string($category_ids)) {
                $category_ids = explode(',', $category_ids);
            }
            $child_category_include = $this->getConfig('child_category_include', 0);
            $include_max_depth = $this->getConfig('include_max_depth', 0);
            if ($child_category_include) {
                $category_ids = $this->_getChildrenCategories($category_ids, $include_max_depth, false);
            }
            $collection->inCategoryFilter($category_ids);
        }
        $helper->isNewFilter($collection, $this->getConfig('is_new_filter', 0));
        $helper->isSpecialFilter($collection, $this->getConfig('is_special_filter', 0));
        $helper->isFeaturedFilter($collection, $this->getConfig('is_featured', 0));
    
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
    
        $order_by = $this->getConfig('order_by');
        $order_dir = $this->getConfig('order_dir', 'ASC');
    
        // SORT & position fallback
        $this->_setOrder($collection, $order_by, $order_dir);
        // $collection->addAttributeToSort('position', 'ASC');
    
        if ($page = $this->getConfig('page')) {
            $collection->setCurPage($page);
        } else {
            $collection->setCurPage(1);
        }
    
        if ($limit = $this->getConfig('product_count')) {
            $collection->setPageSize($limit);
        }
        
        return $collection;
    }
    
    protected function _setOrder (& $collection, $order_by='position', $order_dir='ASC') {
        if (empty($order_dir)) $order_dir='ASC';
        switch ($order_by) {
            case 'created_at':
                $collection->addAttributeToSelect('created_at');
            default:
                $collection->addAttributeToSort($order_by, $order_dir);
                break;
            case 'top_sales':
                $this->_addCountOrdered($collection);
                $collection->getSelect()->order(new Zend_Db_Expr("$order_by $order_dir"));
                break;
            case 'top_views':
                $this->_addCountVisited($collection);
                $collection->getSelect()->order(new Zend_Db_Expr("$order_by $order_dir"));
                break;
            case 'top_rating':
            case 'top_reviews':
                $this->_addReviewsCount($collection);
                $collection->getSelect()->order(new Zend_Db_Expr("$order_by $order_dir"));
                break;
            case 'random':
                $collection->getSelect()->order(new Zend_Db_Expr('RAND()'));
                break;
        }
        
    }
    
    /**
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    protected function _addCountOrdered (& $collection) {
        $order_item_table = Mage::getSingleton('core/resource')->getTableName('sales/order_item');
        $subquery = Mage::getSingleton('core/resource')->getConnection('core_read')
            ->select()
            ->from($order_item_table, array('product_id', 'top_sales' => 'SUM(`qty_ordered`)'))
            ->group('product_id');
            
        $collection->getSelect()->joinLeft(array('top_sales_subquery' => $subquery), 'top_sales_subquery.product_id = e.entity_id');
        return true;
    }
    
    /**
     *
     * @param Mage_Catalog_Model_Resource_Product_Collection $collection
     */
    protected function _addCountVisited (& $collection) {
        
        $reports_event_table = Mage::getSingleton('core/resource')->getTableName('reports/event');
        
        $subquery = Mage::getSingleton('core/resource')->getConnection('core_read')
            ->select()
            ->from($reports_event_table, array('*', 'top_views' => 'COUNT(`event_id`)'))
            ->where('event_type_id = 1')
            ->group('object_id');
        
        $collection->getSelect()->joinLeft(array('top_views_subquery' => $subquery), 'top_views_subquery.object_id = e.entity_id');
        return true;
    }
    
    protected function _addReviewsCount (& $collection) {
        $review_summary_table = Mage::getSingleton('core/resource')->getTableName('review/review_aggregate');
        $collection->getSelect()
            ->joinLeft(
                array('rev' => $review_summary_table),
                'e.entity_id = rev.entity_pk_value AND rev.store_id=' . Mage::app()->getStore()->getId(),
                array(
                    'top_reviews' => 'rev.reviews_count',
                    'top_rating'  => 'rev.rating_summary'
                )
        );
        return true;
    }
    
}
