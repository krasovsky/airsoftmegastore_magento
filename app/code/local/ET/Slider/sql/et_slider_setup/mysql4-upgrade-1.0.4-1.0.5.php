<?php
Mage::log('START SLIDER 1.0.5 SETUP', Zend_Log::DEBUG, 'setup.log', true);
$installer = $this;

$installer->startSetup();

$group_table = $installer->getTable('slider/group');

$installer->getConnection()->addColumn(
    $group_table,
    'identifier',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'length' => 256,
        'nullable' => true,
        'default' => null,
        'comment' => 'Group String Identifier'
    )
);

$installer->endSetup();

Mage::log('END SLIDER 1.0.5 SETUP', Zend_Log::DEBUG, 'setup.log', true);