<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Admin
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$debug = false;
try{
    $slider_groups = Mage::getModel('slider/group')->getCollection();
    if ($slider_groups->getSize() == 0){
        $group = Mage::getModel('slider/group')->setData(array(
            'title' => 'Edge Group',
            'description' => 'Sample Group for Edge Theme',
            'state' => 1,
            'css_position' => 'relative',
            'css_width' => '575px',
            'css_height' => '455px',
            'loading_screen' => 1,
            'lazy_loading' => 1
        ))->save();
        
        if ($group_id = $group->getId()){
            $slides = array(
                array(
                    'title' => 'Slide #1',
                    'description' => 'Sample Slide #1',
                    'state' => 1,
                    'slide_image' => 'et/slider/slide/slide1.jpg',
                    'slide_transition' => 'Doors',
                    'css_position' => 'absolute'
                ),
                array(
                    'title' => 'Slide #2',
                    'description' => 'Sample Slide #2',
                    'state' => 1,
                    'slide_image' => 'et/slider/slide/slide2.jpg',
                    'slide_transition' => 'Fade',
                    'css_position' => 'absolute'
                    ),
                array(
                    'title' => 'Slide #3',
                    'description' => 'Sample Slide #3',
                    'state' => 1,
                    'slide_image' => 'et/slider/slide/slide3.jpg',
                    'slide_transition' => 'Chess Replace TB',
                    'css_position' => 'absolute'
                    )
            );
            foreach ($slides as $slide_data){
                $slide = Mage::getModel('slider/slide')
                    ->setData($slide_data)
                    ->setGroupId($group_id)
                    ->save();
                $debug && var_dump($slide->getData());
            }
            
            $debug && die('done');
        } else {
            $debug && die('no group');
        }
    } else {
        $debug && die('size > 0');
    }
} catch(Exception $e){
    $debug && die($e->getMessage());
}
