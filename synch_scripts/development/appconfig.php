<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * config for layout pattern
 */

$config["appconfig"] =

    array(
        "ImageMagickPath" => "c:\Program Files\ImageMagick-6.9.1-Q16",
        "wkhtmltoimagePath" => "C:\\Program Files (x86)\\wkhtmltopdf\\bin",
        "pngquantPath" => "C:\\xampp\\_support\\pngquant",
    )

?>