<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

set_time_limit(0);

define('MAGENTO', realpath(dirname(dirname(__FILE__))));
require_once MAGENTO . '/app/Mage.php';
Mage::setIsDeveloperMode(true);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$magento = Mage::getSingleton('core/resource')->getConnection('core_write');
$intranet = Mage::getSingleton('core/resource')->getConnection('intranet_write');


$memcache = new Memcache;
$memcache->connect('localhost', 11211) or die ("Could not connect");
$intranetItems = $memcache->get('123');

if (!$intranetItems || true) {
    $queryStr = <<<T_START_HEREDOC
SELECT
  "admin" AS store,
  "base" AS websites,
  "Default" AS attribute_set,
  "simple" AS TYPE,
  a.category_ids AS category_ids,
  i.sku AS sku,
  i.id as item_id,
  0 AS has_options,
  0 AS exclude_from_supply_needs,
  p.`name` AS name,
  p.`seo_link_title` AS meta_title,
  p.`seo_meta_description` AS meta_description,
  REPLACE(p.`img`,'|',',') AS gallery,
  p.`vanity_url` AS url_key,
  p.vanity_url AS url_path,
  pp.price AS price,
  p.`backend_name` AS short_description,
  IF(
    p.on_sale = 0,
    "",
    TRUNCATE(
      pp.price - (pp.price * (p.on_sale / 100)),
      2
    )
  ) AS special_price,
  p.`weight` AS weight,
  IF(
    p.merchandisable = 1,
    1,
    2
  ) AS status,
  p.merchandisable,
  "Catalog, Search" AS visibility,
  "Taxable Goods" AS tax_class_id,
  "simple" AS product_type_id,
  0 AS ordered_qty,
  i.allocated_inventory AS reserved_qty,
  p.long_description AS description,
  i.inventory AS qty,
  p.`backend_name` AS product_name,
  1 AS is_in_stock,
  ps.`color`,
	ps.`muzzle_velocity`,
ps.`build_material`,
ps.`length` AS overall_length,
ps.`magazine_capacity`,
ps.`inner_barrel`,
ps.`battery_type`,
i.`UPC` AS upc,
ps.`model`,
ps.`propellant`,
"" AS features,
c.new AS new,
p.retail_price

FROM
  products p
  INNER JOIN `productpricings` pp
    ON p.id = pp.id
  INNER JOIN items i ON i.`product_id` = p.id
  INNER JOIN
	(SELECT p.id,p.sku AS sku , CONCAT(2,',',GROUP_CONCAT(cx.category_id + 2 ORDER BY cx.`category_id` ASC)) AS category_ids
		FROM products p
		INNER JOIN categories_xref cx ON cx.`product_id` = p.id
		GROUP BY p.id
	) a ON a.id = p.id
INNER JOIN productspecs ps ON p.`id` = ps.`product_id`
LEFT JOIN customizer c ON c.id = p.id
WHERE i.sku <> "" and i.processed = 0
ORDER by i.last_update_date asc
LIMIT 1000
T_START_HEREDOC;


    $intranetItems = $intranet->fetchAll($queryStr);
    $memcache->set('123', $intranetItems, false, 100000);
}

if (empty($intranetItems)) {
    var_dump("Nothing to update!");
}

$processedIds = array();
foreach ($intranetItems as $item) {
    //ZEND_DEBUG::dump($item);
    //$item["sku"] .= "111";
    $lastUpdateDate = date("Y-m-d");
    $intranet->query("UPDATE items set processed = 1 , last_update_date = :updateDate where id = :item_id" , array("updateDate" => $lastUpdateDate,"item_id" => $item["item_id"]));
    $product = Mage::getModel("catalog/product")->loadByAttribute('sku', $item["sku"]);
    $cats = explode(',', $item["category_ids"]);
    $status  = ($item["merchandisable"] == 1) ?  Mage_Tag_Model_Tag_Relation::STATUS_ACTIVE : 2;
    var_dump($status);
    var_dump($product);
    if (! $product) {
        var_dump($item);
        $product = Mage::getModel('catalog/product');

        var_dump($cats);
        $product
            ->setAttributeSetId(4) //ID of a attribute set named 'default'
            ->setTypeId('simple') //product type
            ->setCreatedAt(strtotime('now')) //product creation time
            ->setSku($item["sku"]) //SKU
            ->setName($item["name"]) //product name
            ->setWeight($item["weight"])
            ->setStatus($status)//product status (1 - enabled, 2 - disabled)
            ->setTaxClassId(2) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
//            ->setManufacturer(28) //manufacturer id
//            ->setColor(24)
//            ->setNewsFromDate('06/26/2014') //product set as new from
//            ->setNewsToDate('06/30/2014') //product set as new to
//            ->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)

            ->setPrice($item["price"]) //price in form 11.22
            ->setCost($item["price"]) //price in form 11.22
            ->setSpecialPrice(empty($item["special_price"])) //special price in form 11.22
//            ->setSpecialFromDate('06/1/2014') //special price from (MM-DD-YYYY)
//            ->setSpecialToDate('06/30/2014') //special price to (MM-DD-YYYY)
            ->setMsrpEnabled(1) //enable MAP
            ->setMsrpDisplayActualPriceType(1) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
            ->setMsrp($item["retail_price"]) //Manufacturer's Suggested Retail Price

            ->setMetaTitle($item["meta_title"])
            ->setMetaKeyword($item["meta_title"])
            ->setMetaDescription($item["meta_description"])

            ->setDescription($item["description"])
            ->setShortDescription($item["short_description"])
            ->setCategoryIds($item["category_ids"])

            ->setData("muzzle_velocity",$item["muzzle_velocity"])
            ->setData("build_material",$item["build_material"])
            ->setData("overall_length",$item["overall_length"])
            ->setData("battery_type",$item["battery_type"])
            ->setData("inner_barrel",$item["inner_barrel"])
            ->setData("upc",$item["upc"])
            ->setData("model",$item["model"])
            ->setData("new",$item["new"])

            ->setStockData(array(
                    'use_config_manage_stock' => 0, //'Use config settings' checkbox
                    'manage_stock'=>1, //manage stock
                    'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
                    'max_sale_qty'=>50, //Maximum Qty Allowed in Shopping Cart
                    'is_in_stock' => $item["is_in_stock"], //Stock Availability
                    'qty' => $item["qty"] //qty
                )
            );

        $imagesArr = explode(",", $item["gallery"]);

        foreach ($imagesArr as $key => $imageUrl) {
            if(empty($imageUrl)) continue;
            if (!$key == 0) $arrayVal = array('image', 'small_image', 'thumbnail'); else $arrayVal = null;
            $product->addImageToMediaGallery(downloadImage($imageUrl), $arrayVal, false, true);
        }
        $product->save();

    ///UPDATING!!!
    } else {
        $needToSave = false;
        //ZEND_DEBUG::dump($product->getCategoryIds());

        if ($item["category_ids"] != implode(',', $product->getCategoryIds())) {
            var_dump("123");
            var_dumP($item["category_ids"]);
            $product->setCategoryIds($item["category_ids"]);
            $needToSave = true;
        }

        if ($item["name"] != $product->getName()) {
            var_dump(321);
            $product->setName($item["name"]);
            $needToSave = true;
        }

        if ($item["meta_title"] != $product->getMetaTitle()) {
            var_dump("asd");
            $product->setMetaTitle($item["meta_title"]);
            $needToSave = true;
        }

        if ($item["meta_description"] != $product->getMetaDescription()) {
            var_dump("aaa");
            $product->setMetaDescription($item["meta_description"]);
            $needToSave = true;
        }

        if ($item["price"] != $product->getPrice()) {
            var_dump("price");
            $product->setPrice($item["price"]);
            $needToSave = true;
        }

        if ($item["short_description"] != $product->getShortDescription()) {
            $product->setShortDescription($item["short_description"]);
            $needToSave = true;
        }

        if($item["status"] != $product->getStatus()){
            $product->setStatus($item["status"]);
            $needToSave = true;
        }

        if($item["retail_price"] != $product->getMsrp()){
            $product->setMsrp($item["retail_price"]);
            $needToSave = true;
        }

        if($item["muzzle_velocity"] != $product->getData("muzzle_velocity")){
            $product->setData("muzzle_velocity",$item["muzzle_velocity"]);
            $needToSave = true;
        }

        if($item["build_material"] != $product->getData("build_material")){
            $product->setData("build_material",$item["build_material"]);
            $needToSave = true;
        }

        if($item["overall_length"] != $product->getData("overall_length")){
            $product->setData("overall_length",$item["overall_length"]);
            $needToSave = true;
        }

        if($item["battery_type"] != $product->getData("battery_type")){
            $product->setData("battery_type",$item["battery_type"]);
            $needToSave = true;
        }

        if($item["inner_barrel"] != $product->getData("inner_barrel")){
            $product->setData("inner_barrel",$item["inner_barrel"]);
            $needToSave = true;
        }

        if($item["upc"] != $product->getData("upc")){
            $product->setData("upc",$item["upc"]);
            $needToSave = true;
        }

        if($item["model"] != $product->getData("model")){
            $product->setData("model",$item["model"]);
            $needToSave = true;
        }

        if($item["new"] != $product->getData("new")){
            $product->setData("new",$item["new"]);
            $needToSave = true;
        }

        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
        var_dump($stockItem->getQty());

        if ($stockItem->getQty() != $item["qty"]) {
            $stockItem->setQty($item["qty"]);
            $stockItem->setIsInStock(($item["qty"] > 0 ? 1 : 0));
            $stockItem->save();
        }

        //$mediaGalleryBackendModel = $product->getAttribute('media_gallery')->getBackend();
        //$images = $attributes["media_gallery"];


        $imagesArr = explode(",", $item["gallery"]);
        $imgPath = Mage::getBaseDir('media') . DS . "catalog" . DS . "product" . DS . "i" . DS . "m" . DS . "image_";
        $hasLarge = false;

        foreach ($imagesArr as $key => $imageUrl) {
            if(empty($imageUrl))    continue;

            //var_dump($imageUrl);
            $imageNameArr = explode('/', $imageUrl);
            $imageName = $imageNameArr[count($imageNameArr) - 1];
            //var_dump($imgPath.$imageName);

            if (file_exists($imgPath . $imageName)) {
                $hasLarge = true;
                var_dump("has image " . $imageName);
                continue;
            }
            $needToSave = true;
            var_dump("downloading image " . $imageUrl);

            if (!$hasLarge) $arrayVal = array('image', 'small_image', 'thumbnail'); else $arrayVal = null;
            $product->addImageToMediaGallery(downloadImage($imageUrl), $arrayVal, false, false);
        }
        var_dump($needToSave);
        if ($needToSave) {
            $product->save();
        }
    }

    $idString = implode(',',$processedIds);

    var_dump("COMPLETE!");
}

function downloadImage($imageUrl){

    $imageNameArr = explode('/', $imageUrl);
    $imageName = $imageNameArr[count($imageNameArr) - 1];

    $importPath = Mage::getBaseDir('media') . DS . 'import' . DS . "image_" . $imageName;

    $ch = curl_init($imageUrl);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $rawdata = curl_exec($ch);
    curl_close($ch);

    if (file_exists($importPath)) {
        unlink($importPath);
    }
    if (empty($rawdata)) throw new Exception("Enable to download image ==> {$imageUrl}");

    $fp = fopen($importPath, 'x');
    fwrite($fp, $rawdata);
    fclose($fp);

    return $importPath;
}
