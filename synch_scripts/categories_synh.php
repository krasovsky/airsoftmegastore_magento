<?php

/**
 * INTRANET_CATEGORY_ID = MAGENTO_CATEGORY_ID + 2
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

set_time_limit(0);

define('MAGENTO', realpath(dirname(dirname(__FILE__))));
require_once MAGENTO . '/app/Mage.php';
Mage::setIsDeveloperMode(true);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$magento = Mage::getSingleton('core/resource')->getConnection('core_read');
$intranet = Mage::getSingleton('core/resource')->getConnection('intranet_write');


$queryStr = <<<HEREODOC
SELECT
  2 AS rootid,
  "default" AS store,
  c.id + 2 AS category_id,
  c.`name` AS name,
  IF(
    a.name IS NOT NULL,
    CONCAT(
      IF(
        b.name IS NOT NULL,
        CONCAT(
          IF(
            e.name IS NOT NULL,
            CONCAT(e.name, '/'),
            ''
          ),
          b.name,
          '/'
        ),
        ''
      ),
      a.`name`,
      '/',
      c.name
    ),
    c.name
  ) AS categories,
  c.long_desc AS description,
  CONCAT(c.id + 2,'-',c.`vanity_url`) AS url_key,
  IF( c.active = 0, 1, 0) AS is_active,
  c.`title` AS meta_title,
  CONCAT(c.id + 2,'-',c.`vanity_url`,".html") AS url_path,
  1 AS is_anchor,
  c.`meta_keywords` AS meta_keywords,
  c.`meta_description` AS meta_description,
  "PRODUCTS" AS display_mode,
  "" AS page_layout,
  "" AS cms_block,
  "" AS custom_layout_update,
  "" AS custom_design,
  "" AS category_image,
  "" AS category_thumb_image,
  1 AS include_in_menu,
  "" AS custom_apply_to_products,
  "" AS custom_use_parent_settings,
  1 AS position,
  c.parent_id + 2 as parent_id
FROM
  categories c
  LEFT JOIN categories a
    ON c.parent_id = a.id
  LEFT JOIN categories b
    ON b.id = a.`parent_id`
  LEFT JOIN categories e
    ON e.id = b.`parent_id`
 ORDER BY c.id asc
HEREODOC;

$intranetCategories = $intranet->fetchAll($queryStr);
if(empty($intranetCategories)){
    var_dump("Nothing to update!");
}
$idsArr = array();
foreach ($intranetCategories as &$cat) {
    $isFound = false;
    $level = count(explode('/',$cat["categories"]))+ 1;
    $magentoCat = Mage::getModel('catalog/category')->load($cat["category_id"]);
    //$intranet->query("delete from categories_changes where id = {$cat["change_id"]}");
    $catId = $magentoCat->getId();
    if (! $catId) {
        $category = Mage::getModel('catalog/category');
        $category->setId($cat["category_id"]);
        $category->setName($cat["name"]);
        $category->setUrlKey($cat["url_key"]);
        $category->setIsActive(1);
        $category->setDisplayMode('PRODUCTS');
        $category->setIsAnchor(1); //for active achor
        $category->setStoreId(1);
        $category->setDescription($cat["description"]);
        $category->setMetaKeywords($cat["meta_keywords"]);
        $category->setMetaDescription($cat["meta_description"]);
        $parentCategory = Mage::getModel('catalog/category')->load($cat["parent_id"]);
        $category->setPath($parentCategory->getPath().'/'.$cat["category_id"]);
        $category->setLevel($level);
        $category->save();
        var_dump("INSERTING");
        continue;
    }
    else {
        $needToSave = false;
        if ($cat["name"] != $magentoCat->getName()) {
            var_dump($magentoCat->getName());
            var_dump("new: " . $cat["name"]);
            $magentoCat->setName($cat["name"]);
            $needToSave = true;
        }
        if($cat["parent_id"] != 2) {
            var_dump($magentoCat->getParentId());
            var_dump($magentoCat->getPath());
            var_dump($cat["parent_id"]);
            $tparentCategory = Mage::getModel('catalog/category')->load($cat["parent_id"]);
            var_dump($tparentCategory->getPath());
            //die();

        }
        $parentCategory = Mage::getModel('catalog/category')->load($cat["parent_id"]);
        var_dump($parentCategory->getId());
        if ($magentoCat->getPath() != $parentCategory->getPath().'/'.$cat["category_id"]) {
            var_dump("PARENT".$cat["parent_id"]);
            var_dump($magentoCat->getParentId());
            var_dump("UPDATING PATH");
            $parentCategory = Mage::getModel('catalog/category')->load($cat["parent_id"]);
            var_dump($parentCategory->getId());
            if (!$parentCategory->getId()) {
                DIE("PARENT CATEGORY DOESNT EXIST : " . $cat["parent_id"]);
            }

            $magentoCat->setPath($parentCategory->getPath().'/'.$cat["category_id"]);
            $magentoCat->save();
            //$magentoCat->setNamePath($cat["categories"]);
            $needToSave = true;
            //die("1223");

        }

        if($level != $magentoCat->getLevel()){
            $magentoCat->setLevel($level);
            $needToSave = true;
        }

        if ($cat["description"] != $magentoCat->getDescription()) {
            var_dump($magentoCat->getDescription());
            var_dump("new: " . $cat["description"]);
            $magentoCat->setDescription($cat["description"]);
            $needToSave = true;
        }

        if ($cat["meta_keywords"] != $magentoCat->getMetaKeywords()) {
            var_dump($magentoCat->getMetaKeywords());
            var_dump("new: " . $cat["meta_keywords"]);
            $magentoCat->setMetaKeywords($cat["meta_keywords"]);
            $needToSave = true;
        }

        if ($cat["meta_description"] != $magentoCat->getMetaDescription()) {
            var_dump($magentoCat->getMetaDescription());
            var_dump("new: " . $cat["meta_description"]);
            $magentoCat->setMetaDescription($cat["meta_description"]);
            $needToSave = true;
        }


        if ($needToSave) {
            var_dump("NEED TO UPDATE!" . $magentoCat->getId());
            $magentoCat->save();
        }

    }
}
var_dump("SYNC IS COMPLETE");