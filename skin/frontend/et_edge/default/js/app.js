jQuery(function($){
	var isBootstrapEvent = false;
    if (window.jQuery) {
        var all = jQuery('*');
        jQuery.each(['hide.bs.dropdown', 
            'hide.bs.collapse', 
            'hide.bs.modal', 
            'hide.bs.tooltip'], function(index, eventName) {
            all.on(eventName, function( event ) {
                isBootstrapEvent = true;
            });
        });
    }
    
    var originalHide = Element.hide;
    Element.addMethods({
        hide: function(element) {
            if(isBootstrapEvent) {
                isBootstrapEvent = false;
                return element;
            }
            return originalHide(element);
        }
    });

	//show more megamenu
	$i = 0;
	$('.home_style2 .menu-nav-ver .menu-nav-list > li.level-1').each(function(){
	$this = $(this);
		$i ++; 
		if( $i > 7){ 
			$this.addClass('first-off');
		}			
	});	
	$('.home_style3 .menu-nav-ver .menu-nav-list > li.level-1').each(function(){
	$this = $(this);
		$i ++; 
		if( $i > 7){ 
			$this.addClass('first-off');
		}			
	});	
	$j = 0;
	$('.home_style1 .menu-nav-ver .menu-nav-list > li.level-1').each(function(){
		$this = $(this);
		$j ++; 
		if( $j > 8){ 
			$this.addClass('first-off');
		}			
	});	
	$('.menu-nav-ver .menu-showmore').click(function(){
		$this = $(this);
		if( $('.menu-nav-ver .menu-nav-list').hasClass('menu-full')){
			$('.menu-nav-ver .menu-nav-list').removeClass('menu-full');
			$('.show-off', $this).css({'display':'none'});
			$('.show-on', $this).css({'display':'block'});
			$('.menu-nav-ver .menu-nav-list > li.level-1.first-off').css({'display':'none'});
		} else {
			$('.menu-nav-ver .menu-nav-list').addClass('menu-full');
			$('.show-off', $this).css({'display':'block'});
			$('.show-on', $this).css({'display':'none'});
			$('.menu-nav-ver .menu-nav-list > li.level-1.first-off').css({'display':'block'});
		}		
	});	
	
	//show more category css menu
	$i = 0;
	$('.home_style2 .nav-container #nav > li.level-top').each(function(){
	$this = $(this);
		$i ++; 
		if( $i > 7){ 
			$this.addClass('first-off');
		}			
	});	
	$('.home_style3 .nav-container #nav > li.level-top').each(function(){
	$this = $(this);
		$i ++; 
		if( $i > 7){ 
			$this.addClass('first-off');
		}			
	});	
	$j = 0;
	$('.home_style1 .nav-container #nav > li.level-top').each(function(){
	$this = $(this);
		$j ++; 
		if( $j > 8){ 
			$this.addClass('first-off');
		}			
	});	
	$('.nav-container .menu-showmore').click(function(){
		$this = $(this);
		if( $('.nav-container #nav').hasClass('menu-full')){
			$('.nav-container #nav').removeClass('menu-full');
			$('.show-off', $this).css({'display':'none'});
			$('.show-on', $this).css({'display':'block'});
			$('.nav-container #nav > li.level-top.first-off').css({'display':'none'});
		} else {
			$('.nav-container #nav').addClass('menu-full');
			$('.show-off', $this).css({'display':'block'});
			$('.show-on', $this).css({'display':'none'});
			$('.nav-container #nav > li.level-top.first-off').css({'display':'block'});
		}		
	});
	
	$('.products-grid .item').matchHeight({
	    byRow: true,
	    property: 'height',
	    target: null,
	    remove: false
	});;

	//category search
	$('.category-search-content').jqTransform();
try{
	//slide home-happy-client	
	$('.content-happy-client').owlCarousel({
		pagination: false,
		center: false,
		nav: false,
		loop: true,
		margin: 0,
		navText: [ '', '' ],
		slideBy: 1,
		autoplay: false,
		autoplayTimeout: 2500,
		autoplayHoverPause: true,
		autoplaySpeed: 800,
		startPosition: 0, 
		responsive:{
			0:{
				items:1
			},
			480:{
				items:1
			},
			768:{
				items:1
			},
			1200:{
				items:1
			}
		}
	});	
	
}catch(eee){console.log(eee);}
try{
	//slide brand
	$('.block-brand-content').owlCarousel({
		pagination: false,
		center: false,
		nav: true,
		loop: true,
		margin: 20,
		navText: [ '', '' ],
		slideBy: 1,
		autoplay: false,
		autoplayTimeout: 2500,
		autoplayHoverPause: true,
		autoplaySpeed: 800,
		startPosition: 0, 
		responsive:{
			0:{
				items:1
			},
			480:{
				items:1
			},
			768:{
				items:4
			},
			1200:{
				items:6
			}
		}
	});	
	
}catch(eee){console.log(eee);}

try{
//releated products	 
	$('.releated-product-list').owlCarousel({
		pagination: false,
		center: false,
		nav: true,
		navRewind: false,
		loop: false,
		margin: 30,
		navText: [ '', '' ],
		slideBy: 1,
		autoplay: false,
		autoplayTimeout: 2500,
		autoplayHoverPause: true,
		autoplaySpeed: 800,
		startPosition: 0, 
		responsive:{
			0:{
				items:1
			},
			480:{
				items:1
			},
			768:{
				items:3
			},
			1200:{
				items:4
			}
		}
	});
	
}catch(eee){console.log(eee);}
try{
//upsell products 	
	$('.up-sell-list').owlCarousel({
		pagination: false,
		center: false,
		nav: true,
		navRewind: false,
		loop: false,
		margin: 30,
		navText: [ '', '' ],
		slideBy: 1,
		autoplay: false,
		autoplayTimeout: 2500,
		autoplayHoverPause: true,
		autoplaySpeed: 800,
		startPosition: 0, 
		responsive:{
			0:{
				items:1
			},
			480:{
				items:1
			},
			768:{
				items:3
			},
			1200:{
				items:4
			}
		}
	});
}catch(eee){
	console.log(eee);
}	

	if ( $('#meganav').length ){
		var dropdowns = $('#meganav .level-1>.dropdown-menu');
		var cont = $('#meganav').parents('.header-menu').find('>.container');
		var reposTimeout, locking = false;
		var repos = function(){
			var cl = cont.offset().left, cw = cont.outerWidth(), num = dropdowns.length;
			var ow0 = cont.data('ow0'), ow1 = cont.data('ow1');
			if (typeof ow0 == 'undefined') {
				cont.data('ow0', cw);
			} else {
				if (typeof ow1 == 'undefined') {
					ow1 = ow0;
				}
				if ( ow1 == cw ){
					return;
				}
				cont.data('ow1', cw);
			}

			dropdowns.each(function(){
				var dn = $(this);
				if (dn.parent().hasClass('dropdown-full')) return;
				// restore left, width 
				if (dn.data('cssleft')) {
					dn.css('left', dn.data('cssleft'));
				} else {
					dn.data('cssleft', dn.css('left'));
				}
				if (dn.data('csswidth')) {
					dn.css('width', dn.data('csswidth'));
				} else {
					dn.data('csswidth', dn.css('width'));
				}

				var dnoLeft = dn.offset().left;
				if (dn.outerWidth()>cw){
					dn.css('width', cw);
				}

				if ( dn.outerWidth() >= cw ){
					var toleft = (cl-dnoLeft)-(dn.outerWidth() - cw)/2;
				} else if (dnoLeft < cl) {
					var toleft = cl-dnoLeft;
				} else if (dnoLeft+dn.outerWidth()>cl+cw){
					var toleft = cl - (0-cw) - dnoLeft - dn.outerWidth();
				}
				if (typeof toleft != 'undefined'){
					dn.css('left', toleft);
				}
				locking == --num == 0;
			});
		}
		$(repos);
		$(window).on('resize orientationchange', function(){
			if (reposTimeout){
				clearTimeout(reposTimeout);
				reposTimeout = null;
			}
			reposTimeout = setTimeout(repos, 30);
		});
	}
	
	jQuery(window).load(function(){
		jQuery(document).trigger('product-media-loaded');
	});
});	