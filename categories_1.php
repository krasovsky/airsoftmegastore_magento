<?php
	define('MAGENTO', realpath(dirname(__FILE__)));
	require_once MAGENTO . '/app/Mage.php';
	Mage::app();

	$category = Mage::getModel ( 'catalog/category' );
	$tree = $category->getTreeModel ();
	$tree->load ();
	
	$ids = $tree->getCollection ()->getAllIds ();
	
	if ($ids) {
		$file = "var/import/catwithid.csv";
		file_put_contents($file,"catId, catNamen");
		foreach ( $ids as $id ) {
		  $string = $id . ', ' .$category->load($id)->getName() . "n";
			file_put_contents($file,$string,FILE_APPEND);
		}
	}
?>